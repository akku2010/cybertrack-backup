import { Component, OnInit } from "@angular/core";
import { NavController, NavParams, AlertController, ViewController, ToastController, IonicPage, ActionSheetController, LoadingController, Platform } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../../providers/api-service/api-service";
import * as moment from 'moment';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { TranslateService } from "@ngx-translate/core";
declare var cordova: any;
@IonicPage()
@Component({
    selector: 'page-add-customer-model',
    templateUrl: './add-customer-modal.html'
})
export class AddCustomerModal implements OnInit {

    addcustomerform: FormGroup;
    customerdata: any = {};
    Customeradd: any;
    submitAttempt: boolean;
    selectDealer: any;
    isSuperAdminStatus: boolean;
    islogin: any;
    dealerdata: any;
    currentYear: any;
    // isDealer: any;
    DlType = [{
        value: 'dl',
        viewValue: "Driving License"
    }, {
        value: 'Adhar',
        viewValue: "Adhar Card"
    }, {
        value: 'PAN',
        viewValue: "PAN Card"
    }, {
        value: 'voterCard',
        viewValue: "Voter ID Card"
    }

    ];
    Documentdata: any;
    Documentdatashow: any;
    DocumentdataAdhar: any;
    lastImage: string = null;
    Imgloading: any;
    countryCodeArray: any = [];

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public apicallCustomer: ApiServiceProvider,
        public alerCtrl: AlertController,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public actionSheetCtrl: ActionSheetController,
        public file: File,
        public filePath: FilePath,
        public camera: Camera,
        public transferObj: TransferObject,
        public transfer: Transfer,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        private translate: TranslateService
    ) {
        this.getCountryCode();
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isSuperAdminStatus = this.islogin.isSuperAdmin
        var tempdate = new Date();
        tempdate.setDate(tempdate.getDate() + 365);

        this.currentYear = moment(new Date(tempdate), 'DD-MM-YYYY').format("YYYY-MM-DD");

        // this.addcustomerform = formBuilder.group({
        //     userId: ['', Validators.required],
        //     Firstname: ['', Validators.required],
        //     LastName: ['', Validators.required],
        //     emailid: [this.islogin.account, [Validators.required, Validators.email]],
        //     contact_num: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
        //     password: ['', Validators.required],
        //     confpassword: [''],
        //     address: ['', Validators.required],
        //     ExipreDate: [this.currentYear, Validators.required],
        //     dealer_firstname: [''],
        //     DlNo: [""],
        //     Name: [""],
        //     std_code: ["", Validators.required]
        // })

        this.addcustomerform = formBuilder.group({
            userId: ['', Validators.required],
            Firstname: ['', Validators.required],
            LastName: [''],
            emailid: [''],
            contact_num: [''],
            password: ['', Validators.required],
            confpassword: ['', Validators.required],
            address: [''],
            ExipreDate: [this.currentYear],
            dealer_firstname: [''],
            DlNo: [""],
            Name: [""],
            std_code: ["", Validators.required]
        })
    }

    get frm() { return this.addcustomerform.controls; }

    getCountryCode() {
        this.apicallCustomer.getCountryCode().subscribe(data => {
            this.countryCodeArray = data.countries.map(c=>{
              console.log("cheack cd", c);
              var t =  {
                code : c.code,
                name : c.name,
                text : c.code + " " +  c.name
              }
              return t;
            });

            console.log("countries", this.countryCodeArray)
        })
    }

    ngOnInit() {
        if (this.isSuperAdminStatus) {
            this.getAllDealers();
        }
    }

    DocumentOnChnage(type) {
        this.Documentdata = type;
        if (this.Documentdata.value == 'dl') {
            this.Documentdatashow = this.Documentdata.value;
        } else if (this.Documentdata.value == 'Adhar') {
            this.DocumentdataAdhar = this.Documentdata.value;
        }

    }

    // Create a new name for the image
    private createFileName() {
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
        return newFileName;
    }

    // Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    // Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }


    public uploadImage() {
        // Destination URL
        var url = this.apicallCustomer.mainUrl + "users/uploadImage";

        var targetPath = this.pathForImage(this.lastImage);
        console.log("TargetPath=>", targetPath);

        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "photo",
            fileName: filename,
            chunkedMode: false,
            mimeType: "image/jpeg",
            params: { 'fileName': filename }
        };
        // multipart/form-data"
        this.transferObj = this.transfer.create();

        this.Imgloading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.Imgloading.present();

        this.transferObj.upload(targetPath, url, options).then(data => {
            this.Imgloading.dismissAll();
        }, err => {
            console.log("uploadError=>", err)
            this.lastImage = null;
            this.Imgloading.dismissAll();
            this.presentToast('Error while uploading file, Please try again !!!');
        });
    }

    public takePicture(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            // Special handling for Android library
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                    });
            } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            }
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }

    public presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }

    dealerOnChnage(dealer) {
        console.log(dealer);
        //this.dealerdata = dealer;
        this.dealerdata = this.addcustomerform.value.dealer_firstname;
        console.log("dealer", this.dealerdata);
        console.log("dealer id=> " + this.dealerdata.dealer_id);
    }
    onSelect(text) {
        console.log("country selected: ", text)
    }



    addcustomer() {
        this.submitAttempt = true;
        // console.log(devicedetails);
        if (this.addcustomerform.valid) {
            debugger;
            if (this.addcustomerform.value.password === this.addcustomerform.value.confpassword) {
                if (this.islogin.isSuperAdmin == true) {
                    this.customerdata = {
                        "first_name": (this.addcustomerform.value.Firstname ? this.addcustomerform.value.Firstname : null),
                        "last_name": (this.addcustomerform.value.LastName ? this.addcustomerform.value.LastName : null),
                        "email": (this.addcustomerform.value.emailid ? this.addcustomerform.value.emailid : null),
                        "phone": (this.addcustomerform.value.contact_num ? this.addcustomerform.value.contact_num : null),
                        "password": (this.addcustomerform.value.password ? this.addcustomerform.value.password : null),
                        "isDealer": false,
                        "custumer": true,
                        "status": true,
                        "user_id": this.addcustomerform.value.userId,
                        "address": (this.addcustomerform.value.address ? this.addcustomerform.value.address : null),
                        "supAdmin": this.islogin._id,
                        "std_code": (this.addcustomerform.value.std_code ? this.addcustomerform.value.std_code : null),
                        "expdate": (this.addcustomerform.value.ExipreDate ? this.addcustomerform.value.ExipreDate : this.currentYear)
                    }
                } else {
                    if (this.islogin.isDealer == true) {
                        this.customerdata = {
                            "first_name": (this.addcustomerform.value.Firstname ? this.addcustomerform.value.Firstname : null),
                            "last_name": (this.addcustomerform.value.LastName ? this.addcustomerform.value.LastName : null),
                            "email": (this.addcustomerform.value.emailid ? this.addcustomerform.value.emailid : null),
                            "phone": (this.addcustomerform.value.contact_num ? this.addcustomerform.value.contact_num : null),
                            "password": (this.addcustomerform.value.password ? this.addcustomerform.value.password : null),
                            "isDealer": this.islogin.isDealer,
                            "custumer": true,
                            "status": true,
                            "user_id": this.addcustomerform.value.userId,
                            "address": (this.addcustomerform.value.address ? this.addcustomerform.value.address : null),
                            "supAdmin": this.islogin.supAdmin,
                            "std_code": (this.addcustomerform.value.std_code ? this.addcustomerform.value.std_code : null),
                            "expdate": (this.addcustomerform.value.ExipreDate ? this.addcustomerform.value.ExipreDate : this.currentYear)

                            // "Dealer": this.islogin.Dealer_ID['_id']
                        }
                    }
                }

                if (this.dealerdata != undefined) {
                    this.customerdata.Dealer = this.dealerdata.dealer_id;
                } else {
                    this.customerdata.Dealer = this.islogin._id;
                }

                this.apicallCustomer.startLoading().present();
                this.apicallCustomer.signupApi(this.customerdata)
                    .subscribe(data => {
                        this.apicallCustomer.stopLoading();
                        // if (data.message === 'Email ID or Mobile Number already exists') {
                        //     this.toast(data.message);
                        // }
                        if (data.message != undefined) {
                            this.toast(data.message);
                        }

                        // this.Customeradd = data;

                    },
                        err => {
                            this.apicallCustomer.stopLoading();
                            var body = err._body;
                            var msg = JSON.parse(body);
                            var namepass = [];
                            namepass = msg.split(":");
                            var name = namepass[1];
                            let alert = this.alerCtrl.create({
                                message: name,
                                buttons: [this.translate.instant('Okay')]
                            });
                            alert.present();
                        });
            } else {
                let toast = this.toastCtrl.create({
                    message: 'Password and confirm password mismatched... Please try again',
                    duration: 2000,
                    position: 'middle'
                });
                toast.present();
                return;
            }

        }
    }

    toast(msg) {
        let toast;
        if (msg === 'Registered') {
            toast = this.toastCtrl.create({
                // message: msg,
                message: this.translate.instant('Customer added successfully'),
                position: 'middle',
                duration: 1500
            });
            toast.onDidDismiss(() => {
                this.viewCtrl.dismiss();
            });

            toast.present();
        } else {
            toast = this.toastCtrl.create({
                message: msg,
                // message: this.translate.instant('dealeradded', { value: this.translate.instant('cust') }),
                position: 'middle',
                duration: 1500
            });
            toast.onDidDismiss(() => {
                this.viewCtrl.dismiss();
            });

            toast.present();
        }



    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    getAllDealers() {
        var baseURLp = this.apicallCustomer.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        let toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.apicallCustomer.getAllDealerCall(baseURLp)
            .subscribe(data => {
                this.selectDealer = data;
                console.log("dealers list", this.selectDealer)
                // toast.dismiss();
            },
                error => {
                    console.log(error)
                });
    }
}



